/**
 * Bài 1: Tính tiền lương nhân viên
 * luongNgay = 100.000
 * soNgay = 26
 * luong = luongngay x soNgay = 26 x 100.000 = 2.600.000
 */

var luongNgay = 100000;
var soNgay;
var luong;
// nhập số ngày
soNgay = 26;
// tính tiền lương
luong = luongNgay * soNgay;
// xuất console
console.log("Bài 1: Lương của:", soNgay, "ngày là:", luong);

/**
 * Bài 2: Tính giá trị trung bình của 5 số
 * số 1 (b2Num1) = 10;
 * số 2 (b2Num2) = 20;
 * số 3 (b2Num3) = 30;
 * số 4 (b2Num3) = 40;
 * số 5 (b2Num3) = 50;
 * giá trị trung bình (b2Trungbinh) = (số 1 + số 2 + số 3 + số 4 + số 5)/5 = 30
 */

var b2Num1;
var b2Num2;
var b2Num3;
var b2Num4;
var b2Num5;
var b2Trungbinh;
// nhập 5 số cần tính
b2Num1 = 10;
b2Num2 = 20;
b2Num3 = 30;
b2Num4 = 40;
b2Num5 = 50;
// tính trung bình
b2Trungbinh = (b2Num1 + b2Num2 + b2Num3 + b2Num4 + b2Num5) / 5;
// xuất console
console.log("Bài 2: Giá trị trung bình =", b2Trungbinh);

/**
 * Bài 3: Quy đổi tiền USD sang VND
 * Tỷ giá VND/USD (exRate) = 23.500
 * số tiền USD cần chuyển (usdAmount) = 5
 * số tiền VND quy đổi (vnAmount) = exRate x usdAmount = 5 x 23.500 = 117.500 VND
 */

var exRate = 23500;
var usdAmount;
var vnAmount;

// nhập số tiền USD cần quy đổi
usdAmount = 5;
// tính số điền VNĐ quy đổi
vnAmount = exRate * usdAmount;
// xuất console
console.log(
  "Bài 3: Số tiền VND quy đổi của",
  usdAmount,
  "USD là",
  vnAmount,
  "VND, (tỷ giá",
  exRate,
  ")"
);

/**
 * Bài 4: Tính diện tích, chu vi hình chữ nhật
 * Cạnh dài (b4Dai) = 20 cm
 * Cạnh rộng (b4Rong) = 10 cm
 * Diện tích (b4Dientich) =  dài x rộng = 20 x 10 = 200 cm2
 * Chu vi (b4Chuvi) = (dài + rộng) x 2 = (20 +10) x 2 = 60 cm
 */

var b4Dai;
var b4Rong;
var b4Dientich;
var b4Chuvi;
// nhập chiều dài
b4Dai = 20;
// nhập chiều rộng
b4Rong = 10;
// tính diện tích
b4Dientich = b4Dai * b4Rong;
// tính chu vi
b4Chuvi = (b4Dai + b4Rong) * 2;
// xuất console Diện tích
console.log("Bài 4: Diện tích:", b4Dientich);
// xuất console Chu vi
console.log("Bài 4: Chu vi:", b4Chuvi);

/**
 * Bài 5: Tính tổng 2 ký số của số có 2 chữ số
 * Nhập vào số có 2 chữ số (b5Num) = 48
 * Chữ số hàng chục (b5Hangchuc) = 4
 * Chữ số hàng đơn vị (b5Hangdv) = 8
 * Tổng 2 ký số (b5Tong) = số hàng chục + số hàng đơn vị = 4 + 8 = 12
 */

var b5Num;
var b5Hangchuc;
var b5Hangdv;
var b5Tong;
// nhập số có 2 chữ số
b5Num = 48;
// lấy ký số hàng đơn vị
b5Hangdv = b5Num % 10;
// lấy ký số hàng chục
b5Hangchuc = (b5Num - b5Hangdv) / 10;
// tính tổng 2 ký số
b5Tong = b5Hangchuc + b5Hangdv;
// xuất console giá trị tổng 2 ký số
console.log("Bài 5: Tổng 2 ký số của số", b5Num, "là", b5Tong);
